# Role Name
Install and configure Apache on a host running `apt`.


## Requirements
- `apt` package manager
	- For instance, Debian or Ubuntu.


## Role Variables
```yaml
httpd_sites:
  - name: example
    domain: example.com
    serveralias: www.example.com
    documentroot: "{{ httpd_documentroot }}/example"
    # ssl: # uncomment and provide cert names if SSL is desired
    #  key: example.key
    #  cert: example.crt

httpd_service_name: apache2
httpd_port: 80
httpd_ssl_port: 443
httpd_ssl_path: /etc/ssl
httpd_user: www-data
httpd_group: www-data
httpd_documentroot: /var/www
httpd_logs: "/var/log/{{ httpd_service_name }}"

httpd_package: apache2 # example: Debian='apache2'; Arch/RHEL='httpd'
httpd_modules:
  - rewrite
  - ssl

httpd_vhosts_enabled_path: "/etc/{{ httpd_service_name }}/sites-enabled"
httpd_vhosts_available_path: "/etc/{{ httpd_service_name }}/sites-available"

httpd_vhost_conf_template: vhost.conf.j2
httpd_vhost_ssl_conf_template: vhost_ssl.conf.j2
```


## Example Playbook
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
---
- name: Application servers
  hosts: app
  roles:
    - role: robr3rd.apache
	  vars:
	    httpd_sites:
	      - name: my-site
	        domain: my-site.com
	        serveralias: www.my-site.com
	        documentroot: "{{ httpd_documentroot }}/my-site"
```


## License
BSD